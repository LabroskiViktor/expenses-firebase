import React, { Component } from 'react';
import './Config/config';
import firebase from 'firebase';
import './App.css';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import GoogleButton from 'react-google-button';
import Navbar from './components/Navbar';
import DateForm from './components/DateForm';
import AddExpenseModal from './components/AddExpenseModal';
import EditExpensesModal from './components/EditExpensesModal';
import ListExpenses from './components/ListExpenses';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      showModal: false,
      showModalMontlyData: false,
      expenseFormId: '',
      editExpenseFormId: '',
      singleMonthData: {},
      currentUser: null,
      providerData: {}
    };

    this.signOut = this.signOut.bind(this);
    this.getExpenses = this.getExpenses.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.modalBackdropClicked = this.modalBackdropClicked.bind(this);
    this.expenseSubmit = this.expenseSubmit.bind(this);
    this.expenseEditSubmit = this.expenseEditSubmit.bind(this);
    this.editExpense = this.editExpense.bind(this);
    this.openModal = this.openModal.bind(this);
    this.showAlert = this.showAlert.bind(this);
    this.addExpenseDate = this.addExpenseDate.bind(this);
    this.deleteExpenses = this.deleteExpenses.bind(this);
  }

  showAlert(message) {
    Alert.success(message, {
      position: 'bottom-right',
      effect: 'slide',
      offset: 100
    });
  }

  closeAlert() {
    Alert.closeAll();
  }

  signIn() {
    const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(googleAuthProvider);
  }

  signOut() {
    if (this.state.currentUser) {
      firebase.auth().signOut();
      this.setState({ currentUser: null });
    }
  }

  componentDidMount() {
    if (!this.state.currentUser) {
      firebase.auth().onAuthStateChanged(currentUser => {
        if (currentUser) {
          this.setState({
            currentUser: currentUser.uid,
            providerData: currentUser.providerData[0]
          });
          this.getExpenses();
        }
      });
    } else {
      this.getExpenses();
    }
  }

  getExpenses() {
    firebase
      .database()
      .ref(`users/${this.state.currentUser}/`)
      .once('value')
      .then(snapshot => {
        this.setState({ data: snapshot.val() });
      })
      .catch(e => {
        console.log(e);
      });
  }

  openModal(id) {
    this.setState({ showModal: true, expenseFormId: id });
  }

  modalBackdropClicked() {
    this.setState({ showModal: false, showModalMontlyData: false });
  }

  closeModal() {
    this.setState({ showModal: false, showModalMontlyData: false });
  }

  addUpdateExpense(data, id) {
    firebase
      .database()
      .ref(`/users/${this.state.currentUser}/${id}/`)
      .update(data)
      .then(() => {
        this.getExpenses();
        let msg;
        if (this.state.editExpenseFormId !== '') {
          msg = 'Expense Updated';
        } else {
          msg = 'Expense Added';
        }
        this.showAlert(msg);
        this.setState({
          showModal: false,
          showModalMontlyData: false,
          editExpenseFormId: ''
        });
      })
      .catch(e => {
        console.log('something happend ', e);
      });
  }

  expenseSubmit(data, expenseId) {
    const typeOfExpense = Object.keys(data)[0];
    const costOfExpense = Object.values(data)[0];
    firebase
      .database()
      .ref(`users/${this.state.currentUser}/${expenseId}`)
      .once('value')
      .then(snapshot => {
        const expenseObj = {};
        expenseObj[typeOfExpense] = costOfExpense;
        expenseObj['Total'] =
          parseInt(snapshot.val().Total, 10) + parseInt(costOfExpense, 10);
        this.addUpdateExpense(expenseObj, expenseId);
      })
      .catch(e => {
        console.log(e);
      });
  }

  addExpenseDate(data, userId) {
    firebase
      .database()
      .ref(`users/${userId}/`)
      .push(data)
      .then(() => {
        this.getExpenses();
        this.showAlert('New Date Added');
      })
      .catch(e => {
        console.log('something happend ', e);
      });
  }

  editExpense(id) {
    firebase
      .database()
      .ref(`/users/${this.state.currentUser}/${id}/`)
      .once('value')
      .then(snapshot => {
        this.setState({
          singleMonthData: snapshot.val(),
          editExpenseFormId: id
        });
        this.setState({ showModalMontlyData: true });
      })
      .catch(e => {
        console.log(e);
      });
  }

  expenseEditSubmit(data, expenseId) {
    this.addUpdateExpense(data, expenseId);
  }

  deleteExpenses(id) {
    firebase
      .database()
      .ref(`/users/${this.state.currentUser}/${id}/`)
      .remove()
      .then(() => {
        this.getExpenses();
        this.showAlert('Expense Was Deleted!');
      })
      .catch(e => {
        console.log('something happend ', e);
      });
  }

  render() {
    return (
      <div className="App">
        {this.state.currentUser ? (
          <Navbar userData={this.state.providerData} signOut={this.signOut} />
        ) : (
          ''
        )}
        {this.state.currentUser ? (
          <div className="container">
            <DateForm
              addDate={this.addExpenseDate}
              userId={this.state.currentUser}
              getExpenses={this.getExpenses}
            />
            <br />

            <ListExpenses
              data={this.state.data}
              editExpense={this.editExpense}
              openModal={this.openModal}
              deleteExpenses={this.deleteExpenses}
            />

            <br />
            <br />

            <AddExpenseModal
              showModal={this.state.showModal}
              expenseFormId={this.state.expenseFormId}
              closeModal={this.closeModal}
              modalBackdropClicked={this.modalBackdropClicked}
              expenseSubmit={this.expenseSubmit}
            />

            <EditExpensesModal
              showModalMontlyData={this.state.showModalMontlyData}
              closeModal={this.closeModal}
              modalBackdropClicked={this.modalBackdropClicked}
              editExpenseFormId={this.state.editExpenseFormId}
              singleMonthData={this.state.singleMonthData}
              expenseEditSubmit={this.expenseEditSubmit}
            />
            <div onClick={this.closeAlert}>
              <Alert />
            </div>
          </div>
        ) : (
          <div className="container-fluid h-100 container-logIn">
            <div className="row h-100 justify-content-center align-items-center card-container">
              <div className="card">
                <div className="card-body">
                  <div className="googleBtn">
                    <GoogleButton onClick={this.signIn.bind(this)} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default App;
