import React, { Component } from 'react';
import Modal from 'react-bootstrap4-modal';

class AddExpenseModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showError: false
    };
  }

  expenseSubmit(event) {
    event.preventDefault();
    const expenseObj = {};
    expenseObj[this.typeOfExpense.value] = this.costOfExpense.value;
    if (this.typeOfExpense.value === '' || this.costOfExpense.value === '') {
      this.setState({ showError: true });
    } else {
      this.setState({ showError: false });
      this.props.expenseSubmit(expenseObj, this.expenseId.value);
      this.typeOfExpense.value = '';
      this.costOfExpense.value = '';
    }
  }

  render() {
    return (
      <Modal
        visible={this.props.showModal}
        onClickBackdrop={this.props.modalBackdropClicked}
      >
        <div className="modal-header">
          <h5 className="modal-title">Add New Expense</h5>
        </div>
        <form onSubmit={this.expenseSubmit.bind(this)}>
          <div className="modal-body">
            <input
              type="hidden"
              value={this.props.expenseFormId}
              ref={input => (this.expenseId = input)}
            />
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                placeholder="Enter Name Of Expense"
                ref={input => (this.typeOfExpense = input)}
              />
              {this.state.showError ? (
                <span className="errMessage">This field is required</span>
              ) : (
                ''
              )}
            </div>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                placeholder="Enter The Cost"
                ref={input => (this.costOfExpense = input)}
              />
              {this.state.showError ? (
                <span className="errMessage">This field is required</span>
              ) : (
                ''
              )}
            </div>
          </div>
          <div className="modal-footer">
            <input type="submit" className="btn btn-info" value="Add" />
            <button
              type="button"
              className="btn btn-secondary"
              onClick={this.props.closeModal}
            >
              Cancel
            </button>
          </div>
        </form>
      </Modal>
    );
  }
}

export default AddExpenseModal;
