import React, { Component } from 'react';

class DateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showError: false
    };
  }

  submitDate(event) {
    event.preventDefault();
    const dateObj = {
      Date: this.date.value,
      Total: '0'
    };
    if (this.date.value === '') {
      this.setState({ showError: true });
    } else {
      this.setState({ showError: false });
      this.props.addDate(dateObj, this.props.userId);
      this.date.value = '';
    }
  }

  render() {
    return (
      <form onSubmit={this.submitDate.bind(this)}>
        <div className="input-group">
          <input
            type="text"
            placeholder="Enter Date"
            className="form-control"
            ref={input => (this.date = input)}
          />
          <div className="input-group-append">
            <input type="submit" className="btn btn-info" value="Enter Date" />
          </div>
        </div>
        {this.state.showError ? (
          <span className="errMessage">This field is required</span>
        ) : (
          ''
        )}
      </form>
    );
  }
}

export default DateForm;
