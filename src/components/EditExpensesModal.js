import React, { Component } from 'react';
import Modal from 'react-bootstrap4-modal';

class EditExpensesModal extends Component {
  expenseEditSubmit(event) {
    event.preventDefault();
    const updatedObj = {};
    let sum = 0;
    Object.keys(this.props.singleMonthData).forEach(key => {
      if (this[`textInput${key}`] !== undefined) {
        updatedObj[key] = this[`textInput${key}`].value;
        sum += parseInt(this[`textInput${key}`].value, 10);
        updatedObj['Total'] = sum;
      }
    });
    this.props.expenseEditSubmit(updatedObj, this.editExpenseId.value);
  }

  render() {
    return (
      <Modal
        visible={this.props.showModalMontlyData}
        onClickBackdrop={this.props.modalBackdropClicked}
      >
        <div className="modal-header">
          <h5 className="modal-title">Update Expenses</h5>
        </div>
        <form onSubmit={this.expenseEditSubmit.bind(this)}>
          <div className="modal-body">
            <input
              type="hidden"
              value={this.props.editExpenseFormId}
              ref={input => (this.editExpenseId = input)}
            />
            {Object.keys(this.props.singleMonthData).map(key => {
              if (key !== 'Date' && key !== 'Total') {
                return (
                  <div className="form-group" key={key}>
                    <label htmlFor={key}>{key}</label>
                    <input
                      type="text"
                      className="form-control"
                      defaultValue={this.props.singleMonthData[key]}
                      ref={input => (this[`textInput${key}`] = input)}
                    />
                  </div>
                );
              } else {
                return <div key={key} />;
              }
            })}
          </div>
          <div className="modal-footer">
            <input type="submit" className="btn btn-info" value="Update" />
            <button
              type="button"
              className="btn btn-secondary"
              onClick={this.props.closeModal}
            >
              Cancel
            </button>
          </div>
        </form>
      </Modal>
    );
  }
}

export default EditExpensesModal;
