import React, { Component } from 'react';

class ListExpenses extends Component {
  render() {
    let data;
    if (this.props.data) {
      data = Object.keys(this.props.data).map(key => {
        return (
          <div key={key} className="expense-container">
            <h4>
              {this.props.data[key].Date} - Total: {this.props.data[key].Total}
              <span
                className="badge badge-pill badge-danger float-right delete-expense"
                onClick={() => this.props.deleteExpenses(key)}
              >
                X
              </span>
            </h4>

            <ul className="list-group list-group-flush">
              {Object.keys(this.props.data[key]).map((value, index) => {
                if (Object.keys(this.props.data[key]).length !== 1) {
                  if (value !== 'Date' && value !== 'Total') {
                    return (
                      <li
                        key={index}
                        className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
                        onClick={() => this.props.editExpense(key)}
                      >
                        {value}
                        <span className="badge badge-info badge-pill">
                          {Object.values(this.props.data[key][value])}
                        </span>
                      </li>
                    );
                  }
                } else {
                  return (
                    <div key={index}>
                      There are no expenses for {this.props.data[key].Date}
                    </div>
                  );
                }
              })}
            </ul>
            <br />
            <button
              className="btn btn-info btn-circle btn-lg"
              onClick={() => this.props.openModal(key)}
            >
              +
            </button>
            <br />
            <br />
            <hr />
            <br />
          </div>
        );
      });
    }
    return <div>{data}</div>;
  }
}

export default ListExpenses;
