import React, { Component } from 'react';

class Navbar extends Component {
  render() {
    const { displayName, photoURL } = this.props.userData;

    return (
      <nav className="navbar navbar-expand-sm bg-info navbar-dark">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <img src={photoURL} alt={displayName} className="rounded-circle" />
          </li>
          <li className="nav-item">
            <a className="nav-link">{displayName}</a>
          </li>
        </ul>
        <a className="nav-link logOut" onClick={this.props.signOut}>
          Log Out
        </a>
      </nav>
    );
  }
}

export default Navbar;
